#
setwd('/Users/Shared/MATLAB/ModernAdmix/')

## Input data
	# Reordering columns and converting from percentages
  prep_input <- function(m){
	m = as.matrix(m)
	m = m*0.01  # convert percentages
	o = order(colnames(m))
	m = m[,o]
  }

 R = read.csv('Harappa1.csv', row.names = 1)
 R <- R[, -ncol(R)]  # last column is NA because of trailing comma
 R <- prep_input(R)
 
 test = read.csv('test.csv', row.names = 1)
 test <- prep_input(test)
 
 	## Compute ancestry
 	source('r/change.r')
 	#result = changeAdmixRef(R, test[2, ])
 	
 	cat(rep('*', 20), "\n")
 	print(result)
 
 	res= result[result[,1]>=0.01,]
 	cat("Most significant ( >0.01 ): \n")
 	print(res)
 	cat("\n")
 	cat("Explains ", sum(res), "\n")
 