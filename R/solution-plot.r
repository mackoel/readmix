

    # ??? somehow does not work


solutionPlot <- function(pred,  t, scale=T){	
	K = length(t)
	o = order(t, decreasing = TRUE)
	t = t[o]
	
	t.color='black'
	sol.color = '#990033'
	cols = c(t.color, sol.color)
	legend_text = c("test(actual)", 'predicted')
	xlabels = names(t)
	
	plot(c(1,K), c(0,max(max(pred), max(t))), 
			 xlab = NA, ylab="Proportion", 
			sub = NA, type='n')
	par(las = 2)
	axis(1, at = 1:K, labels = xlabels)
	par(las = 0)

	lines(pred, col=sol.color, lty=3,  lwd=2, type='o')
	
	lines(t, lwd=3)
	legend('topright', legend = legend_text, fill = cols)
}