
admixturePlot <- function(R, t, scale=T){	
	K = ncol(R)
	I = nrow(R)
	
	## Reorder columns of R and t ?
	#o = order(t, decreasing = TRUE)
	o = order(-colSums(R))
	t = t[o]
	R = R[,o]
	
	
	cols = rainbow(I)[sample(I)]
	xlabels = colnames(R)
	
	plot(c(1,K), c(0,max(max(R), max(t))), 
			axes = FALSE, xlab = NA, ylab="Proportion", sub = NA, type='n')
	par(las = 2)
	axis(1, at = 1:K, labels = xlabels)
	par(las = 0)
	for(ii in 1:I){
		
		if (scale){
			max_s = 1 / max(t/R[ii,]) 
		} else {
			max_s = 1
		}		
		
		#max_s = 1 # min(t/R[ii,])
		lines(R[ii,] * max_s, col=cols[[ii]], lwd=2, type='o')
	}
	lines(t, lwd=3)
	legend('topright', legend = rownames(R), fill = cols)
}