
getInitCoef0 <- function(r,t){
	large <- (r > 0.01)  ##1/length(t))
	eps = 1e-5
	max_prop = min( ((t+eps)/(r+eps))[large] )
}



getScores <- function(R,t){
	score.eps = 1e-6
	K = ncol(R)
	K1 = nrow(R)
	proj_score = rep(0, K1)
	for(i in 1:K1){
		# proj_score[[i]] <- sum( R[i, ]*t )  # Euclidean
		max_s = 1 / max((t+score.eps)/(R[i,]+score.eps))   
		lin_pen = max(t - R[i,])
		# penalty for not explaining maximal component!
		max_prop = min((t+score.eps)/(R[i,]+score.eps))
		  # 
		# max_s = 1/ max( (R[i,]+score.eps) / (t+score.eps) )
				  # note max always > 1
				  # Option 1
		r = R[i,] * max_s * max_prop
		# proj_score[[i]] <- sum( R[i,]*max_prop*t ) + 1*max_prop  -doesn't 
		proj_score[[i]] <- sum( r*t )  #+ 1*max_prop 
		
		#proj_score[[i]] <- - lin_pen*0.00001 + proj_score[[i]]
	
					# Option 2
		#r = R[i,] * max_s  # max_s - "best explanation",
		#proj_score[[i]] <-  - sum( (t-r)^2) + 0.03*max_prop
				
	}
	names(proj_score)<-rownames(R)
	proj_score/sum(proj_score)
}

getRelevantPops <- function(R,t){
	eps = 0.005
	prec.eps = 1e-5
	small_t  = which( t < eps)
	t[small_t] = eps
	R0 = R
	#R[R<eps] = eps
	# R[R>0.5] = R[R>0.5] - eps
	t[-small_t] <- t[-small_t] - eps
	 max_props =  apply( t/( R[,]), 1, min)
	 max_props[max_props > 1] = 1
	 Rsc = max_props * R # using recycling of vectors
	 max_contrib = apply( Rsc, 1, max)
	 #take  = which( max_contrib > 0.02)
	 take  = which( max_props > 0.01)
	 
	 par(mfrow = c(1,2))
	 plot( max_props[take])
	 #print(max_props[take])
	 axis(1, at=1:length(take), labels=rownames(R)[take])
	 plot( max_contrib[take])
	 par()
	 return( R0[take, ])
}

getInitCoef <- function(R, i,  t){
	r = R[i, ]
	t0 = t
	large <- (r > 0.005)  ##1/length(t))
	eps = 1e-5
	t[t<0]=0
	max_prop = min( ((t+eps)/(r+eps))[large] )
	return(max_prop)
	
	##!!!
	steps=200
	theta = max_prop* (0:steps)/steps
	for(j in length(theta):1 ){
		t = t0 - r*theta[[j]]
		sc = getScores(R,t  )
		if(which.max(sc) == i){
			# found the last
			return(theta[[j]])
		}
	}
	return(0)
}

changeAdmixRef <- function(R, t){
	# R is the matrix of new wrt old
	# t is a test vector
	.DEBUG = TRUE
	R  = getRelevantPops(R,t)
	t0 = t
	K = ncol(R)
	K1 = nrow(R)
	if(length(t) != K) {
		stop("Number of admixture components of test sample must be equal to the number of reference populations - the number of columns in R")
	}
	
	# Order the rows
	# compute scores
	proj_score  = getScores(R,t)
	ro = order(proj_score, decreasing = TRUE)
	
	if(.DEBUG){
		pdf(file="row_scores.pdf")
		plot(proj_score[ro])
		dev.off()
	}
	
	# change R
	R = R[ro, ]
	proj_score = proj_score[ro]
	
	#DEBUG
	source('r/admixture-plot.r')
	pdf("closest-pops.pdf")
	admixturePlot(R[1:7,], t)
	dev.off()
	
	#R = R[1,]
	s_rows = 1
	s = getInitCoef(R, 1, t)
	names(s) <- rownames(R)[s_rows]
	s = as.matrix(s)
	cat("First s = "); print(s); cat("\n\n")
	
	#print ( s %*% R[s_rows, ])
	t = t0 - s %*% R[s_rows,]
	cat("\n --- Current t= "); print(t); cat("\n\n")
	plot(t)
	print(sum(t[t<0]))

	desiredPrecision = 0.02
	
	getLossFunction <- function(s_rows, t){
	  function(s){
	  	#print(s)
	  	#cat("\n")
	  	#print(R[s_rows,])
	  	cat("Loss: s= "); print( s ); cat("\n")
	  	cat("s_rows: ", s_rows, "\n")
	  	cat("dim s ")
	  	print(dim(s))
		pred = as.matrix(s) %*% R[s_rows,]
		#cat("\n",  pred, "\n")
		err = sum( (pred-t)^2)
		over = pred > t
		quality_loss =   100*sum( (pred-t)[over] ) + err # +sum(over) 
	  }
	}
	

	#!!!!!###!
	for(i in 2:30){
		cat("\n", rep("*", 20), "\n")
		cat("**     Iteration ", i, "     **\n")
		cat(rep("*", 20), "\n")
		sc = getScores(R, t)  # R[-s_rows]
		o = order(sc, decreasing = T)
		new_row_idx = o[[1]] #o[[ rpois(1,2) ]]
		#new_row_idx = i
		while(new_row_idx %in% s_rows){
			#stop("t didn't change enough")
			cat("sampling \n")
			new_row_idx = sample(K1, 1)
		}
		s_rows = c(s_rows, new_row_idx)
		
		if(F){
		loss = getLossFunction(s_rows, t0)
		
		#s_new = getInitCoef(R[new_row_idx,],t)
		
		#s = c(s, 0)
		steps = 100
		capacity = 1-sum(s)
		cat("Unexplained:")
		print(capacity)
		theta0 = (0:steps/steps)* capacity 
		ses0 = cbind( rep(1,steps+1) %o% s, theta0)
		
		theta1 = capacity +  (1-capacity)*(0:steps)/steps
		
		ses1 =  cbind( (1+ capacity - theta1) %o% s,    theta1  )
		
		theta = theta0
		ses = ses0		 
		#theta = c(theta0, theta1)
		#ses = rbind(ses0, ses1)
		
		losses = apply(ses, 1, loss)
		
		
		hess = diff(losses, diff=2)
		mh = which.max(hess)+2 # where loss starts decreasing markedly slower
		m = which.min(losses)
		#m = as.integer( (m + mh) / 2);
		
		if(F){
			par(mfrow = c(1,2))
			plot(losses)
			plot( diff(losses, diff=1)); abline(h=0)
			par()
		}
		theta0 = theta[[m]] 
		s = ses[m, ]
		
		}
		
		new_coef =  getInitCoef(R, new_row_idx, t) 
		new_coef = as.matrix(new_coef)
		
		s = rbind(s, new_coef )

		#t = t0 - new_coef %*% R[s_rows,]
		t = t - new_coef %*% R[new_row_idx, ]
		plot(t)
		# Scale t ?
		#t = t/sum(t)
		
		t[t<0] = 0
		
		if(F){
		if (losses[m] < desiredPrecision){
			break
		}
		}
		#cat("Current rows:") ; print(s_rows)
		rownames(s) <- rownames(R)[s_rows]
		cat("s= ")
		print(s); cat("\n")
	}
	
	rownames(s) <- rownames(R)[s_rows]
	return(s)	
	
}