plot_pops <- function(R,t){
	eps = 0.005
	prec.eps = 1e-5
	small_t  = which( t < eps)
	t[small_t] = eps
	R0 = R
	#R[R<eps] = eps
	# R[R>0.5] = R[R>0.5] - eps
	t[-small_t] <- t[-small_t] - eps
	 max_props =  apply( t/( R[,]), 1, min)
	 max_props[max_props > 1] = 1
	 Rsc = max_props * R # using recycling of vectors
	 
	 take  = which( max_props > 0.01)
	 
	 par(mfrow = c(1,2))
	 plot( max_props[take])
	 print(max_props[take])
	 axis(1, at=1:length(take), labels=rownames(R)[take])
	 plot( max_contrib[take])
	 par()
	 
	 }
	 
	 t = test[2,]
	 oc = order(t)
	 R = R[,oc]
	 sc = getScores(R,t)
	 o = order(sc, decreasing = T)
	 R = R[o,]
	 
	 admixturePlot(R[1:10,], t)
	 
	  
	 N = nrow(R)
	 #N = ncol(R)
	 N = 40
	 cols = rainbow(N)[sample(N)]
	 
	 #for(i in 1:10){
	 #	lines(R[i,], col = cols[[i]], type='o', pch=15)
	 #}
	 #persp(R[1:10,])
	 
	 plot(c(1,N), c(0,max(max(R), max(t))), type='n')
	  for(i in 1:ncol(R) ){
	 	lines(R[1:N,i], col = cols[[i]], type='o', pch=15)
	 }
	 
	 
	 
	 
	 