
source(paste(DISTPATH, "/R/cluster-pops.R", sep = ""))
source(paste(DISTPATH, "/R/solution-plot.r", sep = ""))
source(paste(DISTPATH, "/R/lpsolve-readmix.R", sep = ""))

source(paste(DISTPATH, "/R/change.r", sep = ""))
source(paste(DISTPATH, "/R/initial-search.R", sep = ""))
source(paste(DISTPATH, "/R/sa.R", sep = ""))
source(paste(DISTPATH, "/R/search-within-similar.R", sep = ""))

final_optimize <- function(R,t0){
	#lpo = lp_max_explained_readmix(R,t0)
	lpo = lp_cheb_approx_readmix(R,t0, alpha = 0)
	#lpo = lp_slack_readmix(R,t0)
	s = lpo$solution
	maxerr = max(abs( pred(s, R)- t0 ))
	c(lpo, maxerr = maxerr)
}


###################################################################
### readmix		The core computation routine
###################################################################
readmix <- 
  function(R,t, 
            init_rows = integer(0), 
						init_s = numeric(0),
						subsetSize = 4,
						search.iter = 1e4, 
						keep.init = FALSE,
						ref.err = NULL,     # reference error tolerances
            R_cl = NULL   # cluster assignments
						){
	t0 = t
	modern_pops = rownames(R)
  
  cat("\n========= readmix.r ========== \n")
  cat("Test vector: "); print(t0)
	cat("Requested solution size: ", subsetSize, "\n")

	### Parameter checking
	# init_rows may be given as a vector of population names
	if(class(init_rows)=="character"){
		ms = match(init_rows, modern_pops)
		# If not everything matched:
		if(any(is.na(ms))){
			cat("Did not found match for: \n")
			for(j in which(is.na(ms))){
				cat(init_rows[[j]], "\n")
			}
			cat("\n")
			stop("Error: unknown population names.\n")
		}
		init_rows = ms
	}

	if (length(keep.init) == 1) keep.init = rep(keep.init, length(init_rows))
	if( subsetSize < sum(keep.init)){
	    stop("Input parameters inconsistency: 
The initial population file requires having at least ", sum(keep.init), "populations; 
however the requested number of populations is ", subsetSize, "\n" )
	}
	
	if(length(init_rows)>0){
		cat("Conditional mode: \n")
		cat("  assume the following populations are admixed:\n")
		#for(j in init_rows){
		for(i in seq(along=init_rows)){
		    j = init_rows[[i]]
			cat(j," ", modern_pops[[j]],  " keep = ", keep.init[[i]] ,"\n")
		}
	}
	s_rows = init_rows
	s = init_s
	
	#  - should explain 95% ?
	#  - should each be present at the level of 6% or higher ?
	#  - should as a whole produce an approximation with max error 3%
	
	old_exp = 0
	iter = 0
	errorlist = numeric(0)
	
	all_names = rownames(R)
	  
  #############  Utility functions
  
	newSolution <- function(s_rows, err){
	  popnames = rownames(R)[s_rows]
	  names(sol) = popnames
	  list(solution = sol, 
	       row = s_rows, 
	       popnames = popnames,  		
	       max.err =  err,       # lpo$maxerr,
	       R.red = R[s_rows,],
	       t = t0
	  )
	}
  
  
	################### Building phase
	

	## Before starting, compute coefficients for supplied pops
	beta_user = 0.65 # how much of possible amount should be assigned
	for(i in seq(along=s_rows)){
	  r = R[ s_rows[[i]], ]
	  new_coef = get_coef(r,t, beta = beta_user)
	  t = t - new_coef * r	
	}
	print(R[s_rows, , drop=FALSE])
  
  ##  Adding new populations
	while( length(s_rows) < subsetSize){
		iter = iter + 1
		cat("----  Iteration ", iter, "\n")
	
		## Rank population vectors according to a "fitness" to t		
		#o = rankR(R,t, fitness)
		
		##### Choose the next vector to subtract: ####
		new_i = findNextVector(R, t, s_rows)
		####
		r = R[new_i,]
		# -- Updating the solution -- #
		# See if we already used it
		ix_old = which(new_i == s_rows)	
		if( length(ix_old) > 0){ # add to an existing component
			#sol[ix_old] = sol[ix_old]  + new_coef
		} else { # add a new componenet			
			s_rows = c(s_rows, new_i)
		}
		
		Rc = R[s_rows, , drop=FALSE]
		if(is.null(nrow(Rc))){
			Rc = matrix(R[s_rows,], nrow = length(s_rows))
		}
		
		############### Finding coefficients ##############
		#### Find the best max error approximation
		lpo = lp_cheb_approx_readmix(Rc,t0)
		sol = lpo$solution
		err = lpo$maxerr
		
		errorlist = c(errorlist, err) # save error for convergence analysis 
		
		## Terminatoin options 
		#if(err < 0.005) break
		#error_reduction = rev(-diff(errorlist))[1]
		#if( !is.na(error_reduction ) 
		#		&& error_reduction  < 0.01 ) break
		
		cat("Current solution: \n")
		print( matrix(sol, dimnames=list(all_names[s_rows], "coefficient") ) )
		
		#####  Updating the target vector ######
		## Approximation based ont the whole solution vector
		
		pr = pred(sol, Rc )
		
		# Updating t based on the last coefficient found
		        ###### COEF ######
		new_coef = get_coef(r,t)
		t = t - new_coef * r
		
		# Option2:  Updating based on the whole current solution
		#t = t0 - pr  
		#cat("t= ", sprintf("%.2f",t), "\n")
		if(min(t) < -0.025) stop("t became too small.. \n")
		 
		 ##  Error - max error
		error = max(abs(t))		
		cat(" unexplained admixture coeffs are <", sprintf("%.2f",error), " \n")
		cat(" Percentage of ancestry explained:", 	sprintf("%.2f",100*sum(sol)), "\n")
		
		# Having more populations than columns in R will cause more indeterminacy than is already here
		if(iter > ncol(R)) break
	} # end of building phase

	#############  When adding new populations is not needed
	if( length(s_rows) >= subsetSize){
	  lpo = lp_cheb_approx_readmix(R[s_rows, ,drop=FALSE],t0)
	  #sol = lpo$solution
	  err = lpo$maxerr
	}
  ############## End of Phase 1
  
	lasterr = err
	
	##---------- Phase 2. Iterative improvement of the solution

	## Check if can return early due to reference error tolerance
	pr = pred( sol, R[s_rows, ,drop=FALSE])
	cat( "Error: ", format( abs(t0 - pr) , digits=2) ,"(cp. ", err, ")",  " \n")
  if(!is.null(ref.err)){
    pr_err = pred(sol, ref.err[s_rows, ,drop=FALSE])
    m = max( abs(t0 - pr) - pr_err)
    cat(" Population tolerance:", format(pr_err, dig=2), "\n")
    cat("Remaining error: ", m, "\n")
    if( m <= 0) return(newSolution(s_rows, err ) )
  }


    ## Deterministic improvement: loopp over possible swaps and use it if error decreases
		s_rows = improve_iterative(R, t0, s_rows, lasterr, 
			ref.error = ref.err,
			keep.init = keep.init)
		
		Rc = R[s_rows, , drop=FALSE]
		lpo = lp_cheb_approx_readmix(Rc, t0, alpha=0)
		lasterr = lpo$maxerr
		
		## Randomized improvement
		s_rows = improve_rand(R, t0, s_rows, lasterr, 
				iter = search.iter, keep.init = keep.init,
				ref.error = ref.err)

  ## Search within related populations, given a clustering
  if(!is.null(R_cl)){
    Rc = R[s_rows, , drop=FALSE]
    lpo = lp_cheb_approx_readmix(Rc, t0, alpha=0)
    lasterr = lpo$maxerr
    
    s_rows = search_within_similar(R, t0, s_rows, lasterr, 
                                   ref.error = ref.err,
                                   keep.init = keep.init , R_cl = R_cl)
  }

	#######
  
		Rc = R[s_rows, , drop=FALSE]
		lpo = final_optimize(Rc,t0)
		sol = lpo$solution
		err = lpo$maxerr
	

	## Wrap up the output
	popnames = rownames(R)[s_rows]
	if(is.null(popnames)) stop("Population names missing! \n")
	names(sol) = popnames
	cat("Final populations:", popnames, "\n")
	cat("coefs: ", sol, "\n")
	ans = list(solution = sol, 
				row = s_rows, 
				popnames = popnames,			
				max.err = lpo$maxerr,
				R.red = R[s_rows,],
				t = t0
				)
	ans
}

within_error <- function(Rsd, s_rows, sol){
	terr = Rsd[s_rows, , drop=FALSE] %*% matrix(sol, ncol=1)
}



improve_rand_1 <- function(R,t0, s_rows, init.error, 
		ref.error = ref.err,
		iter=1e4, keep.init=FALSE,
		alpha = 0){
    #if( length(keep.init) == 1 ) keep.init = rep(keep.init, length(s_rows) )
	lasterr = init.error
	if(lasterr<=0) return(s_rows)
	thresh = 1
	pnames = rownames(R)
	
	if (length(keep.init) < length(s_rows))
	    keep.init = c( keep.init, rep(FALSE, length(s_rows) - length(keep.init)))
	changeable = (1:length(s_rows))
	if( length(keep.init) !=0) changeable =  changeable[!keep.init]
	for(a in 1:iter){
		avail_rows = (1:nrow(R))[-s_rows]	
		# pick a vector
		i = sample(avail_rows, 1)

		new_rows = c(s_rows, i)
		
		Rc = R[new_rows, , drop=FALSE]
		lpo = lp_cheb_approx_readmix(Rc,t0, alpha = alpha)
		sol = lpo$solution
		err = lpo$maxerr
		
			if (err/lasterr < thresh){
				# Try to remove a vector
				errs = rep(Inf, length(s_rows))
				sols = matrix(NA, nrow=length(s_rows), ncol=length(s_rows))
				ms = rep(Inf, length(s_rows))
				for(j in changeable ){
				    # Do not touch populatoins marked "to keep"
				    #if( keep.init[[j]] ) next
					# Remove s_rows[[j]] and see what the error is
					new_rows = c( s_rows[-j], i)
					Rc = R[new_rows, , drop=FALSE]
					if(is.null(rownames(Rc))) stop("Population names missing \n")
					lpo = lp_cheb_approx_readmix(Rc,t0, alpha = alpha)
					errs[[j]] = lpo$maxerr
					sols[,j]= lpo$solution
					sol = lpo$solution
					
					pr = pred(sol, R[s_rows, , drop=FALSE])
					pr_err = pred(sol, ref.error[s_rows, , drop=FALSE])
					m = max( abs(t0 - pr) - pr_err)
					ms[j] = m
				}
				# Effective errors
				errs = ms
				
				j0 = which.min(errs)
				if( errs[[j0]] < lasterr){
					
					cat(" change", pnames[s_rows[[j0]] ], "->", 
					 pnames[i])
					s_rows =  c( s_rows[-j0], i)
					
					cat("\t err ", errs[[j0]], "\n")
					lasterr = errs[[j0]]
					# For very small error, can return early
					if(lasterr < 1e-16) return(s_rows)
					sol = sols[,j0]
					pr = pred(sol, R[s_rows, , drop=FALSE])
					pr_err = pred(sol, ref.error[s_rows, , drop=FALSE])
					m = max( abs(t0 - pr) - pr_err)
					if(m <= 0) return(s_rows)
				}
			}
		#} # end loop over vectors
	} 
	s_rows
}

#improve_rand <- improve_rand_1
improve_rand <- improve_rand_sa



improve_iterative <- function(R,t0, s_rows, init.error, 
				ref.error = ref.err,
				alpha = 0, alpha2 = 0, cond = "free",
				keep.init = FALSE )
{
	if(class(R) !="matrix") stop("R must be a matrix.\n")
	subsetSize <- length(s_rows)
	fixed.coef <- 1 / subsetSize
	thresh = 1.0 # greater: more time but more thorough search
	if (cond == "equal") {
		thresh = 2.0
	}
	changePossible = TRUE
	lasterr = init.error
	pnames = rownames(R)
	if (lasterr <= 0) return(s_rows)
	cat(rownames(t0), " Deterministic improvement - trying all possible 1-populations swaps in order. \n")
# During the construction of the solution, the size of the solution grew but the size of keep.init did not
# So we need to extend keep.init
	if (length(keep.init) < length(s_rows)) keep.init = c( keep.init, rep(FALSE, length(s_rows) - length(keep.init)))
	stopifnot( length(keep.init) == length(s_rows) )
	changeable = (1:length(s_rows))
# Note that keep.init is a logical vector
	if (length(keep.init) != 0) changeable = changeable[!keep.init]
	while(changePossible){
		avail_rows = (1:nrow(R))[-s_rows]
		changePossible = FALSE
# Loop through all vectors and try to replace
		for(i in avail_rows){
			new_rows = c(s_rows, i)
			Rc = R[new_rows, , drop=FALSE]
			if (cond == "free") {
				lpo = lp_cheb_approx_readmix(Rc, t0, alpha = 0)	
				sol = lpo$solution
				err = lpo$maxerr
			} else if (cond == "equal") {
				sol = rep(fixed.coef, (subsetSize + 1))
				pr = pred(sol, Rc)
				err = max( abs(t0 - pr) )
			}
			if (err/lasterr <= thresh ){
# Try to remove a vector
				errs = rep(Inf, length(s_rows))
				sols = matrix(NA, nrow=length(s_rows), ncol=length(s_rows))
				for(j in changeable){
# Remove s_rows[[j]] and see what the error is
					new_rows = c(s_rows[-j], i)
					Rc = R[new_rows, , drop=FALSE]
					if(is.null(rownames(Rc))) stop("Population names missing \n")
					if (cond == "free") {
						lpo = lp_cheb_approx_readmix(Rc,t0, alpha=alpha2)
#						errs[[j]] = lpo$maxerr
						sol = lpo$solution
					} else if (cond == "equal") {
						sol = rep(fixed.coef, subsetSize)
#						pr = pred(sol, Rc)
#						errs[[j]] = max( abs(t0 - pr) )
					}					
					sols[,j]= sol
					pr = pred(sol, R[s_rows, , drop=FALSE])
					if ( !is.null(ref.error) ) {
						pr_err = pred(sol, ref.error[s_rows, , drop=FALSE])
						m = max( abs(t0 - pr) - pr_err)
					} else {
						m = max( abs(t0 - pr) )
					}
					errs[[j]] = m
				}
				j0 = which.min(errs)
				if (errs[[j0]] < lasterr){
					cat(rownames(t)," change ", pnames[s_rows[[j0]]], "->", pnames[i])
					s_rows =  c(s_rows[-j0], i)
					lasterr = errs[[j0]]
					cat(" error ", lasterr, "\n")
# For very small error, can return early
					if(lasterr < 1e-9) return(s_rows)
					sol = sols[,j0]
					pr = pred(sol, R[s_rows, , drop=FALSE])
					if ( !is.null(ref.error) ) {
						pr_err = pred(sol, ref.error[s_rows, , drop=FALSE])
						m = max( abs(t0 - pr) - pr_err)
					} else {
						m = max( abs(t0 - pr) )
					}
					if (m <= 0) return(s_rows)
					changePossible = TRUE
					break
				}
			}
		} # end loop over vectors
	} # end while
	return(s_rows)
}



########################################################
########################################################
########################################################

getLegendText <- function(s, R){
	if(length(s)>1){  #(class(R)=="matrix"){
		popnames =  rownames(R)
	} else { #if R is just a vector
		# could just use this
		popnames =  names(s)
	}
	
	popnames = gsub("_.*", "", popnames)	
	paste(popnames," ", sprintf("%.1f",s*100), "%")	
}

readmix_barplot <- function(obj, ...){
	s = obj$solution
	.readmix_barplot(s, obj$R.red, obj$t, ... )
}

.readmix_barplot <- function(s, R, t,...){
	unexpl = 1 - sum(s)
	unexpl.vec = t - colSums( s*R )
	unexpl.vec[unexpl.vec < 0] = 0
	K = ncol(R)
	ancient_pops = colnames(R)
	
	# Remove very small components
	wh = which(s > 0.001)
	s = s[wh]
	R = R[wh,]
	#browser()
	# Order largest first
	o = order(-s)
	# reverse order for barplot
	od = rev(o)
	#s = s[od]
	#R = R[od,]
	col_palette = sample( rainbow(length(s)) )
	cols = col_palette[o]
	
	Rc = s*R # using that s is a column vector, recycling
	
	# When wh is scalar, Rc is just one (row) vector and does not need reordering
	if(length(wh)>1)
		Rc = Rc[od,]
	
	legend_text = getLegendText(s, R)[o]
	legend_col = col_palette[od]
	#
	# Adding the information about unassigned percentage
	# - only if there is something unexplained for every population
	# i.e. unexpl.vec is positive
	if ((unexpl > 0.01) && all(unexpl.vec >= 0)) {
		Rc = rbind(Rc, unexpl.vec)
		cols = c(cols,"#F0F0F0")
		legend_text = c( legend_text , 
				paste0("Other ", sprintf("%.1f",100*unexpl), "%" ))
		legend_col = c(legend_col, "#F0F0F0")
	}

	  ## Main plot ##
	par(mar = c(12.5, 4.1, 4.1, 2.1))
	M = max(max(Rc),t )*1.1
	xlim = c(1,K*1.2)
	
	plot(xlim, c(0,M), type='n', axes=F, xlab=NA, ylab=NA )
	abline(h = seq(0,M,0.01), col="#dddddd")
	barplot(Rc, col = cols, #legend.text = names(s), 
			ylim = c(0, M), add=T, axes=T, axisnames=F, # ylab="Proportion", 
			...)
	par(las = 2)
	
	axis(1, at = (1:K)*1.2 -0.5, labels = ancient_pops, lwd = 0, 
			lwd.ticks=0)
	par(las = 0)
	legend("topright", legend = legend_text, fill = legend_col)
		
		#### Plotting the true test vector  t ####
	middles = 0.7 + 1.2*(0:(K-1))
	points(middles, t,  type='h', lwd=1)
	points(middles, t, pch=3)
}

############################################################
########################################################
########################################################



