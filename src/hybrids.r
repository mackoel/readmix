#!/usr/bin/Rscript

# Reordering columns and converting from percentages
prep_input <- function(m){
  m = as.matrix(m)	
  rsums <- rowSums(m)
  if (any(rsums <= 0)) 
    stop("Bad input.")
  m <- m/rsums
  #o = order(colnames(m)); m = m[,o]
  return(m)
}

args <- commandArgs(TRUE)

USAGE_STRING = "============= ReMix Testing Script, version Aug 4, 2014 ========================
Usage:

$ Rscript hybrids.r <input>

Arguments:
1 Reference file - a CSV file with mean population admixture
2 Nrepl
*************************************************************************\n
"
#Brittish to Greek Iberian UK

EuropeanOrigins <- c("Abkhazians", "Abkhazian", "Adygei", "Armenian", "Belorusian", "Belorussian", "Bulgaria", "Chechens", "Chechen", "Dane", "Danish", "Estonian", "Finnish", "French Basque", "French", "Georgian", "German", "Greek", "Hungarian", "Iberian", "Ingush", "Italian", "Lithuanian", "Mordvinian", "North Italian", "North_Ossetia", "Orcadian", "Palestinian", "Romania", "Russians", "Sardinian", "Tatars", "Tatar", "Tuscan", "Ukrainian", "UK")

AmericanOrigins <- c("Aymara", "Cabecar", "Colombians", "Hidalgo", "Maya", "Morelos", "Mixe", "Peru", "Pima", "Puerto", "Tepehuano", "Quechua", "QUECHUA", "Queretaro", "Zapotec")


if(length(args) < 1){
  cat(USAGE_STRING)
  stop("Please supply valid arguments\n")
}

cat("ARGS:", args, "\n")

optarg <- 1

# Infile: file that has the test sample as its second row (the first row is the header naming the ancestral populations)
INFILE = args[[optarg]]
optarg <- optarg + 1

Nrepl = as.numeric(args[[optarg]])
optarg <- optarg + 1

Korigs = as.numeric(args[[optarg]])
optarg <- optarg + 1

noise_frac <- 0
if (length(args) > 3) {
	noise_frac <- as.numeric(args[[optarg]])
	optarg <- optarg + 1
}

seed.rng <- 12345
if (length(args) > 4) {
	seed.rng <- as.numeric(args[[optarg]])
	optarg <- optarg + 1
}
set.seed(seed.rng)

SDINFILE = "extract-samples-ref.csv.sdev"
if (length(args) > 5) {
	SDINFILE = args[[optarg]]
	optarg <- optarg + 1
}

index_last <- Korigs + 2
#cat("Test read\n")
#test = read.csv(INFILE, row.names = 0, skip = 1, strip.white = TRUE, blank.lines.skip = TRUE, header = FALSE) 
samplestab = read.csv(INFILE)
#cat("Test read\n")
samplestab[,3:index_last] <- prep_input(samplestab[,3:index_last])
#cat("Test read\n")
cat("Test summary\n")
summary(samplestab)

samplestabsd = read.csv(SDINFILE)
#cat("Test read\n")
#samplestabsd[,2:(index_last - 1)] <- prep_input(samplestabsd[,2:(index_last - 1), drop = FALSE])
#cat("Test read\n")
cat("Test summary\n")
summary(samplestabsd)

European <- apply(samplestab, 1, FUN=function(x) return((strsplit(x[2],"_")[[1]])[1] %in% EuropeanOrigins) )
American <- apply(samplestab, 1, FUN=function(x) return((strsplit(x[2],"_")[[1]])[1] %in% AmericanOrigins) )

samplestab <- cbind(samplestab, European, American)
#write.table(file=paste0(INFILE, "-origins.csv"), samplestab, sep = ",")

esamplestab <- samplestab[European,]
esamplestabsd <- samplestabsd[European,]
nesamplestab <- dim(esamplestab)[1]
cat("Number of Europeans =", nesamplestab, "\n")

asamplestab <- samplestab[American,]
asamplestabsd <- samplestabsd[American,]
nasamplestab <- dim(asamplestab)[1]
cat("Number of Americans =", nasamplestab, "\n")

summary(esamplestab)
summary(asamplestab)

# 50x50 europeans mixture

eparent1 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent2 <- sample(nesamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.5 * (esamplestab[eparent1, 3:index_last] + esamplestab[eparent2, 3:index_last])
sdvec <- 0.5 * (esamplestabsd[eparent1, 2:(index_last - 1)] + esamplestabsd[eparent2, 2:(index_last - 1)])
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
#sapply(1:Nrepl, FUN=function(x) return(sdvec[x,]))
eoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eoffspring_dat)

eoffspring <- esamplestab[eparent1,]
eoffspring[, 3:index_last] <- eoffspring_dat

eparent <- paste(esamplestab[eparent1, 2], esamplestab[eparent2, 2], sep = ";")
esampleid <- paste(esamplestab[eparent1, 1], esamplestab[eparent2, 1], sep = "_")
eoffspring[,1] <- esampleid
eoffspring[,2] <- eparent

summary(eoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-e50x50hyb.csv"), eoffspring, sep = ",")

# 50x25x25 europeans mixture

eparent1 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent2 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent3 <- sample(nesamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.5 * esamplestab[eparent1, 3:index_last] + 0.25 * esamplestab[eparent2, 3:index_last] + 0.25 * esamplestab[eparent3, 3:index_last]
sdvec <- 0.5 * esamplestabsd[eparent1, 2:(index_last - 1)] + 0.25 * esamplestabsd[eparent2, 2:(index_last - 1)] + 0.25 * esamplestabsd[eparent3, 2:(index_last - 1)]
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
eoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eoffspring_dat)

eoffspring <- esamplestab[eparent1,]
eoffspring[, 3:index_last] <- eoffspring_dat

eparent <- paste(esamplestab[eparent1, 2], esamplestab[eparent2, 2], esamplestab[eparent3, 2], sep = ";")
esampleid <- paste(esamplestab[eparent1, 1], esamplestab[eparent2, 1], esamplestab[eparent3, 1], sep = "_")
eoffspring[,1] <- esampleid
eoffspring[,2] <- eparent

summary(eoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-e50x25x25hyb.csv"), eoffspring, sep = ",")

# 25x25x25 europeans mixture

eparent1 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent2 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent3 <- sample(nesamplestab, size = Nrepl, replace = TRUE)
eparent4 <- sample(nesamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.25 * esamplestab[eparent1, 3:index_last] + 0.25 * esamplestab[eparent2, 3:index_last] + 0.25 * esamplestab[eparent3, 3:index_last] + 0.25 * esamplestab[eparent4, 3:index_last]
sdvec <- 0.25 * esamplestabsd[eparent1, 2:(index_last - 1)] + 0.25 * esamplestabsd[eparent2, 2:(index_last - 1)] + 0.25 * esamplestabsd[eparent3, 2:(index_last - 1)] + 0.25 * esamplestabsd[eparent4, 2:(index_last - 1)]
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
eoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eoffspring_dat)

eoffspring <- esamplestab[eparent1,]
eoffspring[, 3:index_last] <- eoffspring_dat

eparent <- paste(esamplestab[eparent1, 2], esamplestab[eparent2, 2], esamplestab[eparent3, 2], esamplestab[eparent4, 2], sep = ";")
esampleid <- paste(esamplestab[eparent1, 1], esamplestab[eparent2, 1], esamplestab[eparent3, 1], esamplestab[eparent4, 1], sep = "_")
eoffspring[,1] <- esampleid
eoffspring[,2] <- eparent

summary(eoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-e25x25x25x25hyb.csv"), eoffspring, sep = ",")

# Euro-Americo mixtures

easamplestab <- rbind(esamplestab, asamplestab)
easamplestabsd <- rbind(esamplestabsd, asamplestabsd)
neasamplestab <- dim(easamplestab)[1]
cat("Number of Euro-Americans =", neasamplestab, "\n")

summary(easamplestab)

# 50x50 ea mixture

eaparent1 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent2 <- sample(neasamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.5 * (easamplestab[eaparent1, 3:index_last] + easamplestab[eaparent2, 3:index_last])
sdvec <- 0.5 * (easamplestabsd[eaparent1, 2:(index_last - 1)] + easamplestabsd[eaparent2, 2:(index_last - 1)])
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
eaoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eaoffspring_dat)

eaoffspring <- easamplestab[eaparent1,]
eaoffspring[, 3:index_last] <- eaoffspring_dat

eaparent <- paste(easamplestab[eaparent1, 2], easamplestab[eaparent2, 2], sep = ";")
easampleid <- paste(easamplestab[eaparent1, 1], easamplestab[eaparent2, 1], sep = "_")
eaoffspring[,1] <- easampleid
eaoffspring[,2] <- eaparent

summary(eaoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-ea50x50hyb.csv"), eaoffspring, sep = ",")

# 50x25x25 ea mixture

eaparent1 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent2 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent3 <- sample(neasamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.5 * easamplestab[eaparent1, 3:index_last] + 0.25 * easamplestab[eaparent2, 3:index_last] + 0.25 * easamplestab[eaparent3, 3:index_last]
sdvec <- 0.5 * easamplestabsd[eaparent1, 2:(index_last - 1)] + 0.25 * easamplestabsd[eaparent2, 2:(index_last - 1)] + 0.25 * easamplestabsd[eaparent3, 2:(index_last - 1)]
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
eaoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eaoffspring_dat)

eaoffspring <- easamplestab[eaparent1,]
eaoffspring[, 3:index_last] <- eaoffspring_dat

eaparent <- paste(easamplestab[eaparent1, 2], easamplestab[eaparent2, 2], easamplestab[eaparent3, 2], sep = ";")
easampleid <- paste(easamplestab[eaparent1, 1], easamplestab[eaparent2, 1], easamplestab[eaparent3, 1], sep = "_")
eaoffspring[,1] <- easampleid
eaoffspring[,2] <- eaparent

summary(eaoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-ea50x25x25hyb.csv"), eaoffspring, sep = ",")

# 25x25x25x25 ea mixture

eaparent1 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent2 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent3 <- sample(neasamplestab, size = Nrepl, replace = TRUE)
eaparent4 <- sample(neasamplestab, size = Nrepl, replace = TRUE)

meansvec <- 0.25 * easamplestab[eaparent1, 3:index_last] + 0.25 * easamplestab[eaparent2, 3:index_last] + 0.25 * easamplestab[eaparent3, 3:index_last] + 0.25 * easamplestab[eaparent4, 3:index_last]
sdvec <- 0.25 * easamplestabsd[eaparent1, 2:(index_last - 1)] + 0.25 * easamplestabsd[eaparent2, 2:(index_last - 1)] + 0.25 * easamplestabsd[eaparent3, 2:(index_last - 1)] + 0.25 * easamplestabsd[eaparent4, 2:(index_last - 1)]
noise <- sapply(1:Nrepl, FUN=function(x) return(rnorm(Korigs, sd = as.vector(t(sdvec[x,])))))
head(noise_frac * t(noise))
eaoffspring_dat <- meansvec + noise_frac * t(noise)
summary(eaoffspring_dat)

eaoffspring <- easamplestab[eaparent1,]
eaoffspring[, 3:index_last] <- eaoffspring_dat

eaparent <- paste(easamplestab[eaparent1, 2], easamplestab[eaparent2, 2], easamplestab[eaparent3, 2], easamplestab[eaparent4, 2], sep = ";")
easampleid <- paste(easamplestab[eaparent1, 1], easamplestab[eaparent2, 1], easamplestab[eaparent3, 1], easamplestab[eaparent4, 1], sep = "_")
eaoffspring[,1] <- easampleid
eaoffspring[,2] <- eaparent

summary(eaoffspring)
write.table(file=paste0(INFILE, "-", noise_frac, "-ea25x25x25x25hyb.csv"), eaoffspring, sep = ",")

