#!/usr/bin/python

import sys

f1 = open(sys.argv[1]).read().splitlines()

m={}
for i in f1:
	x = i.split("\t")
	if x[0] not in m:
		m[x[0]]=[]
	m[x[0]].append(x[1])

f2 = open(sys.argv[2]).read().splitlines()

pop_IDs = f2[3].split(",")[:-1]

if(pop_IDs[0]=="0"):
	exit(1)

outf = open(sys.argv[3],"w")

for i in pop_IDs:
	#print "<<",m[i]
	for j in m[i]:
	#	print "<<<<",j
		outf.write("\""+j+"\"\n")
outf.close()
exit(0)
