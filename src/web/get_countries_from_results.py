#!/usr/bin/python

import urllib2
import sys

m={}
map_file=open(sys.argv[1]).read().splitlines() # map of pop_name -> gps coordinates

map_country_code_2_country_name_continent={}
map2file = open(sys.argv[4]).read().splitlines()
for i in map2file:
	x=i.split(",")
	map_country_code_2_country_name_continent[x[0]]=[x[1],x[2]]

for i in map_file:
	x=i.split(',')
	#print x
	m[x[0]]=[x[1],x[2]]


inf=open(sys.argv[2]).read().splitlines()[0] # file with results of reAdmix run
#pops=inf.split(",")[0].split(";")
#percentages=inf.split(",")[1].split(";")

ppp=inf.split(";")
del ppp[-1]
pops=[]
percentages=[]
for i in ppp:
#	print i.split(",")[0]
#	print i.split(",")[1]
	pops.append(i.split(",")[0].strip())
	percentages.append(i.split(",")[1].strip())

country_codes=[]
for i in pops:
	print i
	a=m[i][0]
	b=m[i][1]
	#print a,b
	response = urllib2.urlopen('http://ws.geonames.org/countryCode?username=martin.triska&lat='+a+'&lng='+b)
	html = response.read().rstrip()
	country_codes.append(html)
	#print html
#fout=open(sys.argv[2],"w")

# Mehedi been here -- Hello Martin, I have changed the TSV to CSV with ';' as end of line indicator.
outf = open(sys.argv[3],"w")  # output file with country codes and percentages
for i in range(0,len(country_codes)):
	print country_codes[i]+","+percentages[i]
	a=country_codes[i]
	if a not in map_country_code_2_country_name_continent:
		map_country_code_2_country_name_continent[a]=["NA","NA"]
	outf.write(a+","+map_country_code_2_country_name_continent[a][0]+","+map_country_code_2_country_name_continent[a][1]+","+percentages[i]+";")
outf.close
