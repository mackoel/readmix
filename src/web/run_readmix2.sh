#!/bin/bash

export LANG=C
DEEP_READMIX_PATH="/RAID6/webroot/readmix20/readmix/src"
DEEP_PATH="/RAID6/webroot/readmix20/bin"
DEEP_LDPATH="/RAID6/webroot/readmix20/lib"

pop_2_contry_details="country_map.csv"

if [ -e .local ]; then
	path_to_input="/RAID6/webroot/readmix20/inputs/"
	key=$1
	keep_init=$2
	r_script="$DEEP_READMIX_PATH/deep-admixture.R"
	id_2_Remix_pop_name="/RAID6/webroot/readmix20/id_2_Remix_pop_name.csv"
	script_to_make_init_pop_list="/RAID6/webroot/readmix20/make_list_initial_populations.py"
	reference_file="/RAID6/webroot/readmix20/reference_set_without_coordinates.csv"
	input_file=$path_to_input$1".input.txt"
	admix_input_file=$path_to_input$1".admix_input.txt"
	admix_initial_pops=$path_to_input$1".init_pops_input.txt"
	output_dir="/RAID6/webroot/readmix20/output/"
	country_codes_2_countries_file="/RAID6/webroot/readmix20/countries.csv"
	pop_2_coordinates="/RAID6/webroot/readmix20/pop_2_coordinates.csv"
	script_to_get_countries_from_results="/RAID6/webroot/readmix20/get_countries_from_results.py"
else
	path_to_input="/RAID6/webroot/newGps/data/solo/"
	key=$1
	keep_init=$2
	r_script="$DEEP_READMIX_PATH/deep-admixture.R"
	id_2_Remix_pop_name="/RAID6/webroot/newGps/userPops_2_backendPops.csv"
	script_to_make_init_pop_list="/RAID6/webroot/newGps/make_list_initial_populations.py"
	reference_file="/RAID6/webroot/newGps/Summary_combined_without_coordinates.csv"
	input_file=$path_to_input$1".input.txt"
	admix_input_file=$path_to_input$1".admix_input.txt"
	admix_initial_pops=$path_to_input$1".init_pops_input.txt"
	output_dir="/RAID6/webroot/newGps/data/output/"
	base_dir="/RAID6/webroot/newGps/"
	country_codes_2_countries_file="/RAID6/webroot/readmix20/countries.csv"
	pop_2_coordinates="/RAID6/webroot/newGps/pop_2_coordinates.csv"
	script_to_get_countries_from_results="/RAID6/webroot/newGps/get_countries_from_results.py"
fi

echo $input_file

echo "POPULATION_ID,NORTHEASTASIAN,MEDITERRANEAN,SOUTHAFRICAN,SOUTHWESTASIAN,NATIVEAMERICAN,OCEANIAN,SOUTHEASTASIAN,NORTHERNEUROPEAN,SUBSAHARANAFRICAN" > $admix_input_file
sed -n '3p' $input_file | sed "s/^/$key, /">> $admix_input_file

$script_to_make_init_pop_list $id_2_Remix_pop_name $input_file $admix_initial_pops &> $base_dir"data/log/"$key".py_log.txt"

if [ $? == "1" ];then
        echo "Initial populations not chosen, will run UNCONDITIONAL MODE"
        admix_initial_pops="NONE"
else   
        echo "Initial populations set"
fi


export DEEP_READMIX_PATH=$DEEP_READMIX_PATH
export PATH=$DEEP_PATH:$PATH
export LD_LIBRARY_PATH=$DEEP_LDPATH:$LD_LIBRARY_PATH
command="$r_script $reference_file $admix_input_file $key $output_dir $admix_initial_pops 2 0 $keep_init"
echo $command
$command > $base_dir"data/log/"$key.R_log.txt 2>$base_dir"data/log/"$key.R_errlog.txt

prediction_file="prediction-"$key".tsv"
country_code_file="countries-"$key".tsv"

#command="$script_to_get_countries_from_results $pop_2_coordinates $output_dir$prediction_file $output_dir$country_code_file $country_codes_2_countries_file"
#echo $command
#$command &> $base_dir"data/log/"$key".py2_log.txt"

./get_countries_from_results.py $pop_2_contry_details $output_dir$prediction_file $output_dir$country_code_file &> $base_dir"data/log/"$key".py2_log.txt"

#  Arguments:
#  1 Reference file - a CSV file with mean population admixture
#  2 Input file
#  3 Job ID
#  4 Output directory
#  5 Initial populations file  - use "" for none
#  6 Number of replications (default 1)
#  7 Subset size ( desired number of populations in the solution) default 4

