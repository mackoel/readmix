#!/usr/bin/Rscript

args <- commandArgs(TRUE)

infile <- args[[1]]
nK <- as.numeric(args[[2]])

samp<- read.csv(infile)
ddd <- aggregate(x=samp[3:(3 + nK - 1)], by=samp[2], FUN=mean)
write.csv(file=paste0(infile,".ref"), ddd, row.names=F)

sdd <- aggregate(x=samp[3:(3 + nK - 1)], by=samp[2], FUN=sd)
msd <- apply(sdd[,2:(2 + nK - 1)],2,mean,na.rm=T)
nssd <- sum(is.na(sdd[2]))
mmsd <- matrix(rep(msd, nssd),nrow=nssd,byrow=T)
sdd[is.na(sdd[2]),2:(2 + nK - 1)] <- mmsd
write.csv(file=paste0(infile,".ref.sdev"), sdd, row.names=F)

#klust <- kmeans(ddd[2:15], 17); klust$betweenss; klust$tot.withinss
#cltab <- cbind(reftab[1],cluster=klust$cluster)
#write.table(file=paste0(reffile,".clust"),cltab,row.names=F,col.names=F)
# k=9 cl=25
# k=14 cl=60
#unique(apply(samp, 1, FUN=function(x) {y=(strsplit(x[2],"_")[[1]])[1]; return((strsplit(y," ")[[1]])[1]  )} ))
samp <- cbind(samp, gpop=apply(samp, 1, FUN=function(x) {y=(strsplit(x[2],"_")[[1]])[1]; return((strsplit(y," ")[[1]])[1]  )} ))
gdd <- aggregate(x=samp[3:(3 + nK - 1)], by=samp[(3 + nK)], FUN=mean)

klust <- kmeans(gdd[2:(2 + nK - 1)], 6); klust$betweenss; klust$tot.withinss
cltab <- cbind(gdd[1],cluster=klust$cluster)
samp <- cbind(samp, gind=apply(samp, 1, FUN=function(x) return(cltab[cltab[1]==x[(3 + nK)],2])))
write.table(file=paste0(infile,".ref.clust"), unique(cbind(samp[2],samp[(3 + nK + 1)])),row.names=F,col.names=F)

